FROM debian:11.0-slim
ARG OPENSEARCH_DOWNLOAD_URL="https://artifacts.opensearch.org/releases/bundle/opensearch/1.0.1/opensearch-1.0.1-linux-x64.tar.gz"

#install some tools
RUN apt update && apt install -y curl wget procps vim

#create user for run opensearch
RUN useradd -u 5000 opensearch

#download opensearch and untar it
RUN wget https://artifacts.opensearch.org/releases/bundle/opensearch/1.0.1/opensearch-1.0.1-linux-x64.tar.gz -O - | tar xz && mv opensearch-* opensearch
WORKDIR /opensearch
RUN chmod 755 -R /opensearch
RUN chown -R opensearch:opensearch /opensearch

USER opensearch:opensearch

VOLUME ["/opensearch/data", "/opensearch/logs","/opensearch/config"]

#opensearch listen port
EXPOSE 9200

ENTRYPOINT ["/bin/bash","-c","/opensearch/opensearch-tar-install.sh"]
